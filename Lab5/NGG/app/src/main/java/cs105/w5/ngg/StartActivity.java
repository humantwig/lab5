package cs105.w5.ngg;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.util.Random;
import java.util.Scanner;

public class StartActivity extends AppCompatActivity {

    private int secretNumber;
    private int userInput;
    private TextView textView;
    private EditText userEditText;
    //TODO: edittext & init
    private Button submit, btn4;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start);

        final Random myRandom = new Random();
        this.submit = findViewById(R.id.submit);
        this.textView = findViewById(R.id.textView1);
        secretNumber = myRandom.nextInt(99) + 1;
        this.userEditText = findViewById(R.id.userEditText);
        this.btn4 = findViewById(R.id.button4);

        btn4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent s = new Intent(StartActivity.this, MainActivity.class);
                startActivity(s);
            }
        });

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                userInput = Integer.parseInt(userEditText.getText().toString());
                if (userInput < 0 || userInput > 100) {
                    textView.setText("The number is not between 1 - 100!");
                }
                else if (userInput == secretNumber) {
                        textView.setText("Correct Answer");
                    } else if (userInput > secretNumber) {
                        textView.setText("To High, Guess again");
                    } else {
                        textView.setText("To Low, Guess again");
                    }

                }
            });

        }

    }

